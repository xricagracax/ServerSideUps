
DROP SCHEMA IF EXISTS chat;
CREATE DATABASE chat;
USE chat;

CREATE TABLE User(
 username VARCHAR(32) PRIMARY KEY,
 password CHAR(64) NOT NULL,
 nome VARCHAR(32) NOT NULL,
 apelido VARCHAR(32) NOT NULL,
 email VARCHAR(32) NOT NULL,
 data DATETIME
);

CREATE TABLE Sala(
 id_sala int not null auto_increment,
 nome_sala varchar(32),
 data datetime,
 primary key (id_sala)
);

CREATE TABLE Imagem(
 id_imagem int not null auto_increment,
 imagem blob,
 primary key (id_imagem)
);

CREATE TABLE UtilizadorSala(
 username varchar(32),
 id_sala int,
 foreign key (id_sala) references Sala(id_sala),
 foreign key (username) references User(username),
 primary key (username,id_sala)
);

#data -> data de registo no servidor
CREATE TABLE Mensagem(
 id_mensagem int not null auto_increment,
 username VARCHAR(32),
 id_sala int,
 mensagem VARCHAR(1024),
 id_imagem int null,
 data datetime,
 validade datetime,
 duracao int,
 primary key (id_mensagem),
 foreign key (id_sala) references UtilizadorSala(id_sala),
 foreign key (id_imagem) references Imagem(id_imagem),
 foreign key (username) references UtilizadorSala(username)
);

CREATE TABLE MensagemRemovida(
 id_mensagem int,
 data datetime,
 primary key (id_mensagem,data)
);

INSERT INTO User VALUES
(UPPER('ricardo'), SHA2('ricardo',256), UPPER('ricardo'), UPPER('graça'), 'ricagraca@hotmail.com', CURRENT_TIMESTAMP),
(UPPER('alexandre'), SHA2('alexandre',256), UPPER('alexandre'), UPPER('fonseca'), 'alexandre@hotmail.com', CURRENT_TIMESTAMP),
(UPPER('andre'), SHA2('andre',256), UPPER('andre'), UPPER('fazendeiro'), 'andre@hotmail.com', CURRENT_TIMESTAMP),
(UPPER('antonio'), SHA2('antonio',256), UPPER('antonio'), UPPER('antonio'), 'antonio@hotmail.com', CURRENT_TIMESTAMP),
(UPPER('gaspar'), SHA2('gaspar',256), UPPER('gaspar'), UPPER('ramoa'), 'gaspar@hotmail.com', CURRENT_TIMESTAMP),
(UPPER('pedro'), SHA2('letmein',256), UPPER('pedro'), UPPER('inacio'), 'pedro@hotmail.com', CURRENT_TIMESTAMP);

INSERT INTO Sala (nome_sala,data) VALUES
('Sala de SI', CURRENT_TIMESTAMP),
('Sala de IHC', CURRENT_TIMESTAMP),
('Sala de PDM', CURRENT_TIMESTAMP),
('Sala de PL', CURRENT_TIMESTAMP),
('Sala do IA', CURRENT_TIMESTAMP);

INSERT INTO UtilizadorSala (username,id_sala) VALUES
(UPPER('alexandre'),1),(UPPER('alexandre'),3),(UPPER('alexandre'),4),
(UPPER('alexandre'),5),(UPPER('andre'),1),(UPPER('andre'),2),
(UPPER('andre'),3),(UPPER('andre'),4),(UPPER('andre'),5),
(UPPER('gaspar'),1),(UPPER('gaspar'),2),(UPPER('gaspar'),3),
(UPPER('gaspar'),4),(UPPER('gaspar'),5),(UPPER('ricardo'),1),
(UPPER('ricardo'),2),(UPPER('ricardo'),3),(UPPER('ricardo'),4),
(UPPER('ricardo'),5),(UPPER('antonio'),1),(UPPER('antonio'),3),
(UPPER('antonio'),5);

INSERT INTO Mensagem  (id_sala,id_imagem,username,mensagem,data,validade,duracao) VALUES
(1,null,UPPER('ricardo'),'Ola, tudo bem?',CURRENT_TIMESTAMP, DATE_ADD(CURRENT_TIMESTAMP, INTERVAL 12 HOUR),1),
(2,null,UPPER('andre'),'Ja fizeste o trabalho de IA?',CURRENT_TIMESTAMP,DATE_ADD(CURRENT_TIMESTAMP, INTERVAL 12 HOUR),1),
(2,null,UPPER('andre'),'Ainda não fiz nada...a',CURRENT_TIMESTAMP,DATE_ADD(CURRENT_TIMESTAMP, INTERVAL 12 HOUR),1),
(3,null,UPPER('andre'),'Como vai isso?',CURRENT_TIMESTAMP,DATE_ADD(CURRENT_TIMESTAMP, INTERVAL 12 HOUR),1),
(3,null,UPPER('andre'),'Forever alone',CURRENT_TIMESTAMP,DATE_ADD(CURRENT_TIMESTAMP, INTERVAL 12 HOUR),1),
(3,null,UPPER('andre'),':(',CURRENT_TIMESTAMP,DATE_ADD(CURRENT_TIMESTAMP, INTERVAL 12 HOUR),1),
(3,null,UPPER('andre'),'Esta sala é o máximo',CURRENT_TIMESTAMP,DATE_ADD(CURRENT_TIMESTAMP, INTERVAL 12 HOUR),1),
(1,null,UPPER('gaspar'),'O que?',CURRENT_TIMESTAMP,DATE_ADD(CURRENT_TIMESTAMP, INTERVAL 12 HOUR),1),
(1,null,UPPER('antonio'),'Vamos jogar matrecos?',CURRENT_TIMESTAMP,DATE_ADD(CURRENT_TIMESTAMP, INTERVAL 12 HOUR),1),
(2,null,UPPER('antonio'),'Ok',CURRENT_TIMESTAMP,DATE_ADD(CURRENT_TIMESTAMP, INTERVAL 12 HOUR),1),
(1,null,UPPER('alexandre'),'Sim',CURRENT_TIMESTAMP,DATE_ADD(CURRENT_TIMESTAMP, INTERVAL 12 HOUR),1);

INSERT INTO Imagem Values(-1,null);

CREATE 
EVENT `apagar_mensagens_que_excedem_validade` 
ON SCHEDULE EVERY 1 SECOND
DO 
DELETE FROM Mensagem WHERE CURRENT_TIMESTAMP >= validade;

DELIMITER $$
CREATE TRIGGER remover_imagem
BEFORE DELETE ON Mensagem
FOR EACH ROW
BEGIN
  INSERT INTO MensagemRemovida(id_mensagem,data) VALUES (old.id_mensagem,old.data);
  DELETE FROM Imagem where id_imagem = old.id_imagem;
END$$
DELIMITER ;
