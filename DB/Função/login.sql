CREATE DEFINER=`root`@`localhost` FUNCTION `LOGIN`(user varchar(32), pass char(64)) RETURNS int
BEGIN
	IF 
	(SELECT COUNT(*) 
		FROM (
			SELECT password 
			FROM User U 
			WHERE user = U.username AND pass = U.password
		) C
	) = 1
	THEN RETURN 1;
    END IF;
RETURN 0;	
END