
package pt.ubi.pdm.f_team.pdmupsquejaminganei.DataBase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.Arrays;
import pt.ubi.pdm.f_team.pdmupsquejaminganei.Class.Mensagem;

public class DataBaseManage{

    private String url;
    private String username;
    private String password;
    private Connection connection;
    
    public DataBaseManage(String url, String username, String password){
        try{
            this.url = url;
            this.username = username;
            this.password = password;
            this.connection = DriverManager.getConnection(url, username, password);
        }
        catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
    }
    
    //Inserir user na base de dados
    public boolean registarUser(String[] row){
        
        User user; 
        Statement statement;
                
        //Assumir que não há problemas de input do user
        try{
            if(row.length == 6){
                user = new User(row[1],row[2],row[3],row[4],row[5]);
                statement = connection.createStatement();
                statement.execute(user.getRegistarQuery());
                return true;
            }
        }
        catch(SQLException ex){
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
        
        return false;
    }
    
    //Verificar se user existe
    public boolean loginUser(String[] row){
        
        ResultSet result_set;

        try{
            if(row.length == 3){
                String sql = "IF (\n" +
                "  (SELECT COUNT(*) \n" +
                "	FROM (\n" +
                "	 SELECT *"+"\n" +
                "	 FROM User U \n" +
                "        WHERE '"+row[1]+"' = U.username AND '"+row[2]+"' = U.password\n" +
                "  ) C\n" +
                " ) = 1) \n" +
                " THEN SELECT 1; " +
                " ELSE SELECT 0;" +
                " END IF;";
                result_set = getResultSet(sql);
                if(result_set.first()){
                    if(result_set.getInt(1) == 1){
                        return true;
                    }
                }
            }
        }
        catch(SQLException ex){
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }

        return false;
        
    }
    
    //Executar statement
    public boolean execute(String sql){
        Statement statement;
        
        try{
            statement = connection.createStatement();
            statement.execute(sql);
            return true;
        }
        catch(SQLException ex){
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
        return false;
    }
    
    public ResultSet getResultSet(String sql) throws SQLException{
        
        Statement statement;
        ResultSet result_set;
        
        statement = connection.createStatement();
        result_set = statement.executeQuery(sql);
        
        return result_set;
        
    }

    public Connection getConnection() {
        return connection;
    }
        
}
