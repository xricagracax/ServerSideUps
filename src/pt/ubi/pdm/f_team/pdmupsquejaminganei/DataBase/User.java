
package pt.ubi.pdm.f_team.pdmupsquejaminganei.DataBase;

public class User {
    
    private String username;
    private String password;
    private String nome;
    private String apelido;
    private String email;
    
    public User(String username, String password, String nome, String apelido, String email){
        this.username = username;
        this.password = password;
        this.nome = nome;
        this.apelido = apelido;
        this.email = email;
    }
    
    public String getRegistarQuery(){
        return 
        "INSERT INTO User VALUES(" + 
            "UPPER('" + username + "'), " +
            "UPPER('" + password + "'), " +
            "UPPER('" + nome + "'), " +
            "UPPER('" + apelido + "'), " +
            "UPPER('" + email + "'), " +
            "CURRENT_TIMESTAMP" + 
        ");";
    }
    
    public String getLoginQuery(){
        return "SELECT LOGIN('" + username + "', '" + password + "');";
    }
    
}
