package pt.ubi.pdm.f_team.pdmupsquejaminganei.Pacote;

import java.io.Serializable;
import java.util.ArrayList;
import pt.ubi.pdm.f_team.pdmupsquejaminganei.Class.Imagem;
import pt.ubi.pdm.f_team.pdmupsquejaminganei.Class.Mensagem;
import pt.ubi.pdm.f_team.pdmupsquejaminganei.Class.Sala;
import pt.ubi.pdm.f_team.pdmupsquejaminganei.Class.Utilizador;
import pt.ubi.pdm.f_team.pdmupsquejaminganei.Class.UtilizadorSala;

public class Pacote implements Serializable{

    private ArrayList<Imagem> imagens;
    private ArrayList<Mensagem> mensagens;
    private ArrayList<Sala> salas;
    private ArrayList<Utilizador> utilizadores;
    private ArrayList<UtilizadorSala> utilizadores_sala;

    public Pacote(){
        this.imagens = new ArrayList<>();
        this.mensagens = new ArrayList<>();
        this.salas = new ArrayList<>();
        this.utilizadores = new ArrayList<>();
        this.utilizadores_sala = new ArrayList<>();
    }

    public void clearAll(){
        imagens.clear();
        mensagens.clear();
        salas.clear();
        utilizadores.clear();
        utilizadores_sala.clear();
    }

    public void adicionarImagem(Imagem imagem){
        imagens.add(imagem);
    }

    public void adicionarMensagem(Mensagem mensagem){
        mensagens.add(mensagem);
    }

    public void adicionarSala(Sala sala){
        salas.add(sala);
    }

    public void adicionarUtilizador(Utilizador utilizador){
        utilizadores.add(utilizador);
    }

    public void adicionarUtilizadorSala(UtilizadorSala utilizador_sala){
        utilizadores_sala.add(utilizador_sala);
    }

    public ArrayList<Mensagem> getMensagens(){
        return mensagens;
    }

    public ArrayList<Sala> getSalas(){
        return salas;
    }

    public ArrayList<Imagem> getImagens(){
        return imagens;
    }

    public ArrayList<Utilizador> getUtilizadores(){
        return utilizadores;
    }

    public ArrayList<UtilizadorSala> getUtilizadores_sala(){
        return utilizadores_sala;
    }

    @Override
    public Pacote clone(){
        Pacote pacote = new Pacote();
        pacote.imagens = (ArrayList)imagens.clone();
        pacote.mensagens = (ArrayList)mensagens.clone();
        pacote.salas = (ArrayList)salas.clone();
        pacote.utilizadores = (ArrayList)utilizadores.clone();
        pacote.utilizadores_sala = (ArrayList)utilizadores_sala.clone();

        return pacote;
    }

}
