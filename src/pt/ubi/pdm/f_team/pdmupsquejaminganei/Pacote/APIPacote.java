
package pt.ubi.pdm.f_team.pdmupsquejaminganei.Pacote;

import java.util.Base64;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import pt.ubi.pdm.f_team.pdmupsquejaminganei.Class.Imagem;
import pt.ubi.pdm.f_team.pdmupsquejaminganei.Class.Mensagem;
import pt.ubi.pdm.f_team.pdmupsquejaminganei.Class.Sala;
import pt.ubi.pdm.f_team.pdmupsquejaminganei.Class.Utilizador;
import pt.ubi.pdm.f_team.pdmupsquejaminganei.Class.UtilizadorSala;
import pt.ubi.pdm.f_team.pdmupsquejaminganei.DataBase.DataBaseManage;

public class APIPacote extends Pacote{
    
    private DataBaseManage database_manage;
    
    public APIPacote(DataBaseManage database_manage){
        super();
        this.database_manage = database_manage;
    }

    public String getPacote(){
        Pacote pacote = (Pacote)this.clone();
        String _pacote_ = serializeObject(pacote);
        return this == null ? "ERRO" : _pacote_;
    }
    
    public void adicionarTudo(String nome, String data){
        try{
            this.getImagens(nome, data);
        }
        catch(SQLException ex){
            System.out.println("-Erro ao ir buscar as imagens");
        }
        try{
            this.getMensagens(nome, data);
        }
        catch(SQLException ex){
            System.out.println("-Erro ao ir buscar as mensagens");
        }
        try{
            this.getSalas(nome);
        }
        catch(SQLException ex){
            System.out.println("-Erro ao ir buscar as salas");
        }
        try{
            this.getUtilizadores(nome, data);
        }
        catch(SQLException ex){
            System.out.println("-Erro ao ir buscar os utilizadores");
        }
        try{
            this.getUtilizadoresSalas(nome);
        }
        catch(SQLException ex){
            System.out.println("-Erro ao ir buscar os utilizadoressalas");
        }

    }
    
    //Convert object to stream output (Serialization), so we can send to server.
    public String serializeObject(Object o){
        String serializedObject;
        try {
            ByteArrayOutputStream bo = new ByteArrayOutputStream();
            ObjectOutputStream so = new ObjectOutputStream(bo);
            so.writeObject(o);
            so.flush();
            serializedObject = Base64.getEncoder().encodeToString(bo.toByteArray());         
            return serializedObject;
        } 
        catch (IOException e) {
            System.out.println("Erro -> " + e.toString());
        }
        return null;
    }
    
    //Convert String to object so this client can receive objects from the other.
    public ObjectInputStream deserializeObject(String redisString) throws IOException{
        byte b[] = Base64.getDecoder().decode(redisString); 
        ByteArrayInputStream bi = new ByteArrayInputStream(b);
        ObjectInputStream si = new ObjectInputStream(bi);
        return si;
    }
    
    //Imagens SQL
    public void getImagens(String nome, String data) throws SQLException{
        
        String sql = "SELECT  *" +
                " FROM Imagem I" +
                " WHERE(I.id_imagem in" +
                " (" +
                " SELECT id_imagem" +
                " FROM Mensagem M" +
                " WHERE M.data >= '"+data+"' AND M.id_sala in " +
                " ("  +
                " SELECT id_sala " +
                " FROM UtilizadorSala S" +
                " WHERE S.username = '"+nome+"'" +
                " )));";
        
        ResultSet result_set = database_manage.getResultSet(sql);
        
        while(result_set.next()){
            //id_mensagem, imagem
            adicionarImagem(new Imagem(
                    result_set.getInt("id_imagem"),
                    result_set.getBytes("imagem")
            ));
        }
        
    }
    
    //Mensagens SQL
    public void getMensagens(String nome, String data) throws SQLException{
        
        String sql = "SELECT  *" +
                " FROM Mensagem M" +
                " WHERE(M.id_sala in" +
                " (" +
                " SELECT id_sala" +
                " FROM UtilizadorSala U" +
                " WHERE U.username = '"+nome+"'" +
                " )" +
                " AND M.data  >= '"+data+"');";
        
        ResultSet result_set = database_manage.getResultSet(sql);
        
        while(result_set.next()){
            //1-id_mensagem 2-username 3-id_sala mensagem id_imagem data validade duracao
            adicionarMensagem(new Mensagem(
                    result_set.getInt("id_mensagem"),
                    result_set.getInt("id_sala"),
                    result_set.getString("username"),
                    result_set.getString("data"),
                    result_set.getString("mensagem"),
                    result_set.getInt("id_imagem"),
                    result_set.getInt("duracao"),
                    result_set.getString("validade")
            ));
        }
        
        sql = "SELECT * FROM MensagemRemovida MR;";
        
        result_set = database_manage.getResultSet(sql);
        
        while(result_set.next()){
            //1-id_mensagem 2-username 3-id_sala mensagem id_imagem data validade duracao
            
            Mensagem mensagem = new Mensagem(
                    result_set.getInt("id_mensagem"),
                    -1,
                    "",
                    result_set.getString("data"),
                    "",
                    -1,
                    -1,
                    ""
            );
            mensagem.setRemover(true);
            adicionarMensagem(mensagem);
        }
        
    }
    
    //Salas SQL
    public void getSalas(String nome) throws SQLException{
        
        String sql = "SELECT *" +
                " FROM Sala S" +
                " WHERE (S.id_sala in" +
                " (" +
                " SELECT id_sala" +
                " FROM UtilizadorSala US" +
                " WHERE US.username = '"+nome+"'" +
                " ));";
        
        ResultSet result_set = database_manage.getResultSet(sql);
        
        while(result_set.next()){
            //id_sala nome_sala data
            adicionarSala(new Sala(
                result_set.getInt("id_sala"),
                result_set.getString("nome_sala"),
                result_set.getString("data")
            ));
        }
        
    }
    
    //Utlizadores SQL
    public void getUtilizadores(String nome, String data) throws SQLException{
        
        String sql = "SELECT *" +
                " FROM User U" +
                " WHERE U.username in" +
                " (SELECT US1.username " +
                " FROM UtilizadorSala US1" + 
                " WHERE US1.id_sala in " +
                " (" +
                " SELECT id_sala" +
                " FROM UtilizadorSala US2" +
                " WHERE US2.username = '"+nome+"'" +
                " )) AND U.username <> '"+nome+"';";
        
        ResultSet result_set = database_manage.getResultSet(sql);
        
        //username email nome e apelido
        while(result_set.next()){
            adicionarUtilizador(new Utilizador(
                result_set.getString("Username"),
                result_set.getString("Email"),
                result_set.getString("Nome"),
                result_set.getString("Apelido")
            ));
        }
        
    }
    
    //UtilizadoresSalas SQL
    public void getUtilizadoresSalas(String nome) throws SQLException{
        
        String sql = "SELECT * " +
                " FROM UtilizadorSala US1" + 
                " WHERE US1.id_sala in " +
                " (" +
                " SELECT id_sala" +
                " FROM UtilizadorSala US2" +
                " WHERE US2.username = '"+nome+"'" +
                " );";
        
        ResultSet result_set = database_manage.getResultSet(sql);

        //username id_sala
        while(result_set.next()){
            adicionarUtilizadorSala(new UtilizadorSala(
                result_set.getString("username"),
                result_set.getInt("id_sala")
            ));
        }
        
    }
    
    public void getUsersWithName() {
        
        String sql = "SELECT * " +
                     "FROM User U;";
        
        try{
            ResultSet result_set = database_manage.getResultSet(sql);
             //username email nome e apelido
            while(result_set.next()){
                adicionarUtilizador(new Utilizador(
                    result_set.getString("Username"),
                    result_set.getString("Email"),
                    result_set.getString("Nome"),
                    result_set.getString("Apelido")
                ));
            }
        }
        catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        
    }
    
    public ArrayList<String> getUser(int id_sala){
        
        String sql = "SELECT username " +
                     "FROM User U " +
                     "WHERE U.username in (Select username FROM UtilizadorSala WHERE id_sala = "+id_sala+");";
        
        ArrayList<String> array = new ArrayList<String>();
        
        try{
            ResultSet result_set = database_manage.getResultSet(sql);
             //username email nome e apelido
            while(result_set.next()){
                array.add(result_set.getString("username").toUpperCase());
            }
        }
        catch(SQLException ex){
            System.out.println(" 1 -> " +ex.getMessage());
        }
        
        return array;
        
    }
    
    public int inserirMensagem(Mensagem mensagem) throws SQLException{
        
        String sql = "INSERT INTO Mensagem  (id_sala,id_imagem,username,mensagem,data,validade,duracao) VALUES\n" +
                "("+mensagem.getId_sala()+","
                   +mensagem.getId_imagem() + ","
                   +"UPPER('"+mensagem.getUsername()+"')"+",'" 
                   +mensagem.getMensagem() + "'," 
                   +"CURRENT_TIMESTAMP" + ",'" 
                   +mensagem.getValidade() + "',"
                   +mensagem.getDuracao() + ");";

        database_manage.execute(sql);
        
        String sql1 = "SELECT id_mensagem, data FROM Mensagem WHERE mensagem = '"+mensagem.getMensagem()+"' \n" +
                      "ORDER BY id_mensagem Desc\n" +
                      "LIMIT 1;";
        
        ResultSet result_set = database_manage.getResultSet(sql1);
        result_set.first();
        
        mensagem.setData(result_set.getString("data"));
        
        return result_set.getInt("id_mensagem");
        
    }
    
    public Sala inserirSala(Sala sala, ArrayList<Utilizador> utilizador) throws SQLException {
        
        int id = -1;
        String sql = "INSERT INTO Sala (nome_sala,data) VALUES\n" +
                "('" + sala.getNome_sala() + "', " + "CURRENT_TIMESTAMP);";
        
        database_manage.execute(sql);
        
        String sql1 = "SELECT id_sala, data FROM Sala WHERE nome_sala = '"+sala.getNome_sala()+"' \n" +
                      "ORDER BY id_sala Desc\n" +
                      "LIMIT 1;";
          
        ResultSet result_set = database_manage.getResultSet(sql1);
        result_set.first();
        
        sala.setId_sala(result_set.getInt("id_sala"));
        sala.setData(result_set.getString("data"));
        
        for(int i=0 ; i<utilizador.size() ; i++){
            System.out.println("Inserir " + utilizador.get(i).getUsername()  + " à sala " + sala.getId_sala());
            sql = "INSERT INTO UtilizadorSala (username,id_sala) VALUE" + "(UPPER('"+utilizador.get(i).getUsername()+"'),"+sala.getId_sala()+");";
            database_manage.execute(sql);
        }
        
        return sala;
        
    }
    
    public int inserirImagem(Mensagem mensagem, Imagem imagem) {
        System.out.println("inseririmagem");
        try{
            String sql = "INSERT INTO Imagem (imagem) VALUES (?);";
            PreparedStatement ps = database_manage.getConnection().prepareStatement(sql);

            System.out.println(imagem.getImagem());

            ps.setBytes(1, imagem.getImagem());
            ps.execute();
            System.out.println("Insert");
            int n_imagem, n_mensagem;

            sql = "SELECT id_imagem FROM Imagem " +
                "ORDER BY id_imagem Desc\n" +
                "LIMIT 1;";

            ResultSet result_set = database_manage.getResultSet(sql);
            result_set.first();
            n_imagem = result_set.getInt("id_imagem");

            mensagem.setId_imagem(n_imagem);
            n_mensagem = inserirMensagem(mensagem);
            mensagem.setId_mensagem(n_mensagem);
            
            return mensagem.getId_sala();
        }
        catch(SQLException e){
            System.out.println("ERRO CRTICICO "+ e.getMessage());
        }
        return -1;
    }
    
    public void apagarMensagem(Mensagem mensagem){
        
        String sql = "DELETE FROM Mensagem WHERE id_mensagem = " + mensagem.getId_mensagem()+ ";";
        
        System.out.println(mensagem.getId_mensagem());
        database_manage.execute(sql);
        
    }
    
    //Imprimir
    public void printRow(ResultSet result_set) throws SQLException{
        ResultSetMetaData rsmd = result_set.getMetaData();
        for(int i=1 ; i<=rsmd.getColumnCount() ; i++){
            System.out.print(result_set.getString(i) + " ");
        }
        System.out.println();
    }
    
}
