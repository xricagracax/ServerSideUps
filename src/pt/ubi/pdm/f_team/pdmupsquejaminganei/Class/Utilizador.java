package pt.ubi.pdm.f_team.pdmupsquejaminganei.Class;

import java.io.Serializable;

public class Utilizador implements Serializable{
    private String username;
    private String email;
    private String nome;
    private String apelido;

    public Utilizador(String username, String email, String nome, String apelido) {
        this.username = username;
        this.email = email;
        this.nome = nome;
        this.apelido = apelido;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getApelido() {
        return apelido;
    }

    public void setApelido(String apelido) {
        this.apelido = apelido;
    }
}
