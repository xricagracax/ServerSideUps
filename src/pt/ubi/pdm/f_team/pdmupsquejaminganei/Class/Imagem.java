package pt.ubi.pdm.f_team.pdmupsquejaminganei.Class;

import java.io.Serializable;

public class Imagem implements Serializable{

    private int id_imagem;
    private byte[] imagem;

    public Imagem(int id_imagem, byte[] imagem) {
        this.id_imagem = id_imagem;
        this.imagem = imagem;
    }

    public int getId_imagem() {
        return id_imagem;
    }

    public void setId_imagem(int id_imagem) {
        this.id_imagem = id_imagem;
    }

    public byte[] getImagem() {
        return imagem;
    }

    public void setImagem(byte[] imagem) {
        this.imagem = imagem;
    }

}
