package pt.ubi.pdm.f_team.pdmupsquejaminganei.Class;

import java.io.Serializable;

public class Sala implements Serializable{

    private int id_sala;
    private String nome_sala;
    private String data;

    public Sala(int id_sala, String nome_sala, String data) {
        this.id_sala = id_sala;
        this.nome_sala = nome_sala;
        this.data = data;
    }

    public Sala(int id_sala, String nome_sala){
        this.id_sala = id_sala;
        this.nome_sala = nome_sala;
    }
    
    public Sala(String nome_sala) {
        this.nome_sala = nome_sala;
        
    }

    public int getId_sala() {
        return id_sala;
    }

    public void setId_sala(int id_sala) {
        this.id_sala = id_sala;
    }

    public String getNome_sala() {
        return nome_sala;
    }

    public void setNome_sala(String nome_sala) {
        this.nome_sala = nome_sala;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
