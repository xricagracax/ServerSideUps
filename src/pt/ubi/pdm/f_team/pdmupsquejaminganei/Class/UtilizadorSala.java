package pt.ubi.pdm.f_team.pdmupsquejaminganei.Class;

import java.io.Serializable;
import java.io.SerializablePermission;

public class UtilizadorSala implements Serializable {

    private String username;
    private int id_sala;

    public UtilizadorSala(String username, int id_sala) {
        this.username = username;
        this.id_sala = id_sala;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getId_sala() {
        return id_sala;
    }

    public void setId_sala(int id_sala) {
        this.id_sala = id_sala;
    }

}
