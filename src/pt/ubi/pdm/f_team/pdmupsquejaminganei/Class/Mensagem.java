package pt.ubi.pdm.f_team.pdmupsquejaminganei.Class;


import java.io.Serializable;
import java.net.URI;

public class Mensagem implements Serializable{

    private int id_mensagem;
    private int id_sala;
    private String username;
    private String data;
    private String mensagem;
    private int id_imagem;
    private int duracao;
    private String validade;
    private boolean remover;

    public Mensagem(int id_mensagem, int id_sala, String username, String data, String mensagem, int id_imagem, int duracao, String validade) {
        this.id_mensagem = id_mensagem;
        this.id_sala = id_sala;
        this.username = username;
        this.data = data;
        this.mensagem = mensagem;
        this.id_imagem = id_imagem;
        this.duracao = duracao;
        this.validade = validade;
        remover = false;
    }

    public boolean getRemover() {
        return remover;
    }

    public void setRemover(boolean remover) {
        this.remover = remover;
    }
    
    public Mensagem(String mensagem, String username, String data) {
        this.username = username;
        this.mensagem = mensagem;
        this.data = data;
    }

    public int getId_sala() {
        return id_sala;
    }

    public void setId_sala(int id_sala) {
        this.id_sala = id_sala;
    }

    public int getId_mensagem() {
        return id_mensagem;
    }

    public void setId_mensagem(int id_mensagem) {
        this.id_mensagem = id_mensagem;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public int getId_imagem() {
        return id_imagem;
    }

    public void setId_imagem(int id_imagem) {
        this.id_imagem = id_imagem;
    }

    public int getDuracao() {
        return duracao;
    }

    public void setDuracao(int duracao) {
        this.duracao = duracao;
    }

    public String getValidade() {
        return validade;
    }

    public void setValidade(String validade) {
        this.validade = validade;
    }

}
