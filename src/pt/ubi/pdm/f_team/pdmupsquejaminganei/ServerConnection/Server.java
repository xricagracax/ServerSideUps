
package pt.ubi.pdm.f_team.pdmupsquejaminganei.ServerConnection;

import pt.ubi.pdm.f_team.pdmupsquejaminganei.ServerConnection.ServerAction;
import java.util.ArrayList;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.BufferedReader;

import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    
    public static void main(String[] args) throws IOException {
        
        ServerSocket server_socket = new ServerSocket(1024);
        System.out.println("EXISTE " + Thread.activeCount() + " THREAD NESTE MOMENTO!");

        do{
            ServerAction server_action = new ServerAction(server_socket.accept());
            server_action.start();
            System.out.println("EXISTE " + Thread.activeCount() + " THREADS NESTE MOMENTO!");
        }while(true);
        
    }
    
}
