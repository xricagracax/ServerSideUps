
package pt.ubi.pdm.f_team.pdmupsquejaminganei.ServerConnection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;
import pt.ubi.pdm.f_team.pdmupsquejaminganei.Class.Mensagem;
import pt.ubi.pdm.f_team.pdmupsquejaminganei.Class.Sala;
import pt.ubi.pdm.f_team.pdmupsquejaminganei.DataBase.DataBaseManage;
import pt.ubi.pdm.f_team.pdmupsquejaminganei.Pacote.APIPacote;
import pt.ubi.pdm.f_team.pdmupsquejaminganei.Pacote.Pacote;

public class ServerAction extends Thread{
    
    //SEMAPHORE PARA SINCRONIZAR AÇÕES DO SERVIDOR
    private static final Semaphore available = new Semaphore(1);
    
    //CLIENTES ONLINE
    private final static ArrayList<ServerAction> CLIENT = new ArrayList<ServerAction>();
    private String user;
    
    //PROPRIEDADES DA BASE DE DADOS
    final String URL = "jdbc:mysql://localhost:3306/chat";
    final String USERNAME = "root";
    final String PASSWORD = "root";
    
    //LIGAÇÕES CLIENTE E BASE DE DADOS
    private final DataBaseManage database_manage;
    private final Socket socket;
    
    //LER
    private final BufferedReader buffered_reader;
    private final InputStreamReader input_stream;
    
    //ESCREVER
    private final PrintStream print_stream;
    private final OutputStream output_stream;

    //SABER SE ESTA ONLINE
    private boolean online;
    
    public ServerAction(Socket socket) throws IOException{
        //SOCKET
        this.socket = socket;
        
        //LER
        input_stream = new InputStreamReader(socket.getInputStream());
        buffered_reader = new BufferedReader(input_stream);
        
        //ESCREVER
        output_stream = socket.getOutputStream();
        print_stream = new PrintStream(output_stream);
              
        //BASE DE DADOS
        database_manage = new DataBaseManage(URL, USERNAME, PASSWORD);
        
    }
    
    @Override
    public void run(){
        //Mensagem nunca vem a NULL, vem com "ERRO"
        String message;
        boolean sucesso = false;
        
        do{
            try{
                message = readMessage();
                sucesso = this.doSomething(message == null ? "SAIR" : message);

                //COMEÇAR TAREFA PENDENTE
                available.release();
                System.out.println(sucesso ? "SUCESSO" : "INSUCESSO");
            }
            catch(IOException | InterruptedException ex){
                sucesso = false;
            }
            catch(Exception ex){
                System.out.println(ex.getMessage());
            }
         
        }while(sucesso);
        
        try {
            socket.close();
        } 
        catch (IOException ex) {
            Logger.getLogger(ServerAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        System.out.println("FECHAR THREAD, " + (Thread.activeCount()-1) + " A FUNCIONAREM");
        
    }
    
    public boolean doSomething(String message) throws IOException, InterruptedException, ClassNotFoundException{
        
        String[] message_split = message.split("[/]");
        
        //BLOQUEIA TAREFAS PENDENTES
        available.acquire();
        
        //IMPRIMIR CAMINHO
        printPath(message_split);
                
        switch(message_split[0]){
            //Caso em que o cliente se quer registar
            case "REGISTAR" : {
                boolean registar = database_manage.registarUser(message_split);

                registar &= registar ? sendMessage("REGISTAR/VALIDO") : sendMessage("REGISTAR/INVALIDO");
                return true;
            }
            //Caso em que o cliente quer fazer login
            case "LOGIN" : {
                boolean login = database_manage.loginUser(message_split);
                
                login &= login ? sendMessage("LOGIN/VALIDO") : sendMessage("LOGIN/INVALIDO");
                if(login){
                    if(!CLIENT.contains(this)){
                        CLIENT.add(this);
                    }
                    System.out.println((this.user = message_split[1].toUpperCase())+" adicionado ao servidor! (n="+CLIENT.size()+")");
                }
                return true;
            }
            //Caso em que o cliente sai do servidor
            case "SAIR" : {
                online = false;
                this.closeSocket(); 
                CLIENT.remove(this);
                return false;
            }
            //Buscar pacote
            case "PACOTE" : {
    
                //Receber, enviar pacotes
                APIPacote api_pacote;
                if(!(message_split.length == 1)){
                    api_pacote = new APIPacote(database_manage);
                    //Enviar toda a informação
                    if(message_split[1].equals("RECEBER")){
                        String pacote;
                        api_pacote.adicionarTudo(message_split[2], message_split[3]);
                        pacote = api_pacote.getPacote();
                        sendMessage("/PACOTE" + "/RECEBER/" + pacote);
                    }
                    //Enviar pacote recebido por user
                    if(message_split[1].equals("ENVIAR")){
                        String pacote;
                        Pacote pacote_enviar = new Pacote();
                        if(message_split[2].equals("MENSAGEM")){
                            try{
                                int tamanho = message_split[0].length() + message_split[1].length() + message_split[2].length() + 3;
                                Mensagem mensagem_enviar;
                                mensagem_enviar = (Mensagem)api_pacote.deserializeObject(message.substring(tamanho, message.length())).readObject();
                                ArrayList<String> users = api_pacote.getUser(mensagem_enviar.getId_sala());
                                System.out.println("Entrou aqui!");
                                int id = api_pacote.inserirMensagem(mensagem_enviar);
                                mensagem_enviar.setId_mensagem(id);
                                pacote_enviar.adicionarMensagem(mensagem_enviar);
                                pacote = api_pacote.serializeObject(pacote_enviar);
                                ArrayList<String> lista = api_pacote.getUser(mensagem_enviar.getId_sala());
                                for(int i=0 ; i<CLIENT.size() ; i++){
                                    if(lista.contains(CLIENT.get(i).getUser().toUpperCase())){
                                        System.out.println("Enviar mensagem para " + CLIENT.get(i).getUser().toUpperCase());
                                        CLIENT.get(i).sendMessage("/PACOTE" + "/RECEBER/" + pacote);
                                    }
                                }
                            }
                            catch(SQLException ex){
                                System.out.println(ex.getSQLState());
                            }
                        }
                        if(message_split[2].equals("SALA")){
                            try{
                                int tamanho = message_split[0].length() + message_split[1].length() + message_split[2].length() + 3;
                                pacote_enviar = (Pacote)api_pacote.deserializeObject(message.substring(tamanho, message.length())).readObject();
                                Sala sala = api_pacote.inserirSala(pacote_enviar.getSalas().get(0),pacote_enviar.getUtilizadores());
                                pacote_enviar.getSalas().get(0).setData(sala.getData());
                                pacote_enviar.getSalas().get(0).setId_sala(sala.getId_sala());
                                pacote = api_pacote.serializeObject(pacote_enviar);
                                ArrayList<String> lista = api_pacote.getUser(sala.getId_sala());
                                for(int i=0 ; i<CLIENT.size() ; i++){
                                    if(lista.contains(CLIENT.get(i).getUser().toUpperCase())){
                                        System.out.println("Enviar mensagem para " + CLIENT.get(i).getUser().toUpperCase());
                                        CLIENT.get(i).sendMessage("/PACOTE" + "/RECEBER/" + pacote);
                                    }
                                }
                            }
                            catch(SQLException ex){
                                System.out.println(ex.getMessage());
                            }
                        }
                        if(message_split[2].equals("IMAGEM")){
                            /*try*/{
                                System.out.println("Inserir Imagem");
                                int tamanho = message_split[0].length() + message_split[1].length() + message_split[2].length() + 3;
                                pacote_enviar = (Pacote)api_pacote.deserializeObject(message.substring(tamanho, message.length())).readObject();
                                int id = api_pacote.inserirImagem(pacote_enviar.getMensagens().get(0),pacote_enviar.getImagens().get(0));
                                pacote = api_pacote.serializeObject(pacote_enviar);
                                ArrayList<String> lista = api_pacote.getUser(id);
                                System.out.println(lista);
                                for(int i=0 ; i<CLIENT.size() ; i++){
                                    if(lista.contains(CLIENT.get(i).getUser().toUpperCase())){
                                        System.out.println("Enviar mensagem para " + CLIENT.get(i).getUser().toUpperCase());
                                        CLIENT.get(i).sendMessage("/PACOTE" + "/RECEBER/" + pacote);
                                    }
                                }
                            }
                            /*catch(SQLException ex){
                                System.out.println("1->" + ex.getSQLState());
                                System.out.println("y-> " + ex.toString());
                            }*/
                        }
                    }
                    if(message_split[1].equals("APAGAR")){
                        String pacote;
                        Pacote pacote_apagar = new Pacote();
                        int tamanho = message_split[0].length() + message_split[1].length() + 2;
                        System.out.println("Entrou aqui para apagar");
                        pacote_apagar = (Pacote)api_pacote.deserializeObject(message.substring(tamanho, message.length())).readObject();
                        Mensagem mensagem_apagar = pacote_apagar.getMensagens().get(0);
                        api_pacote.apagarMensagem(mensagem_apagar);
                        pacote = api_pacote.serializeObject(pacote_apagar);

                        ArrayList<String> lista = api_pacote.getUser(mensagem_apagar.getId_sala());
                        for(int i=0 ; i<CLIENT.size() ; i++){
                            if(lista.contains(CLIENT.get(i).getUser().toUpperCase())){
                                System.out.println("Enviar mensagem para " + CLIENT.get(i).getUser().toUpperCase());
                                CLIENT.get(i).sendMessage("/PACOTE" + "/APAGAR/" + pacote);
                            }
                        }

                    }
                    //Enviar Users
                    if(message_split[1].equals("USER")){
                        String pacote;
                        api_pacote.getUsersWithName();
                        pacote = api_pacote.getPacote();
                        sendMessage("/PACOTE/USER/" + pacote);
                    }
                }
                return true;
            }
            
            default : break;
        }
                
        return false;
    }
    
    //Enviar mensagem para o socket deste Objeto
    public boolean sendMessage(String message){
        print_stream.println(message == null ? "" : message);
        return true;
    }
    
    //Receber uma mensagem do socket deste Objeto
    public String readMessage(){
        String message;
        
        try{message = buffered_reader.readLine();}
        catch(IOException ex){return null;}
        
        return message == null ? "SAIR" : message;
    }
    
    public void printPath(String[] message_split){
        for(int i=0 ; i<message_split.length ; i++){
            System.out.print(message_split[i]);
            if(i != message_split.length-1){
                System.out.print(" > ");
            }
        }
        System.out.println();
    }
    
    public void closeSocket() throws IOException{
        //FECHAR OS BUFFER'S
        input_stream.close();
        output_stream.close();
        
        //FECHAR AS STREAMS
        buffered_reader.close();
        print_stream.close();
        
        //FECHAR SOCKET
        socket.close();
        
        System.out.print("FECHAR THREAD > ");
    }
    
    public String getUser(){
        return user;
    }
    
}
